 const express = require ('express');
 const mongoose = require ('mongoose');
 const dotenv = require ('dotenv');
 dotenv.config();

 const app = express();
 const PORT = 3007;

//Middlewares
app.use(express.json())
app.use(express.urlencoded({extened:true}))

//mongoose connection
//mongoose.connect (<connection.string, {options})

	mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});
//DB connection notification

const db = mongoose.connection
db.on ("error", console.error.bind(console, 'connection errot'))
db.once("open", () => console.log(`Connected to database`))

const userSchema = new mongoose.Schema(
    {
        username: {
            type: String,
            required: [true, `Name is required`]
        },
        password: {
            type: String,
            
        }
    }
)

const User = mongoose.model(`User`, userSchema)

app.post('/signup', (req,res) => {
    //console.log(req.body) //object

    User.findOne({username: req.body.username}).then((result, err) => {
        console.log(result)
        //if the task already exisy in the database, we return a message `duplicate not found`
        if(result != null && result.username == req.body.username){
            return res.send(`Duplicate Task found`)
        } else {

            //if the task doesnt exist in the database, we add it in the database
            let newUser = new User({
                username: req.body.username
            })

             return res.send(`New user registered`)

            //use save() method to insert the new document in the database
            newUser.save().then((result,err) =>{
                //console.log(result)
                if (result){
                    return res.send(result)
                } else {
                    return res.status(500).send(err)
                }
            })
        }
    })
    
})
 app.listen(PORT,()=> console.log(`Server connected at PORT ${PORT}`) )